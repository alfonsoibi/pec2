﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverSeta : MonoBehaviour
{
    //Cuando termina la animacion de salir la seta, hacemos que el padre se mueva, esto se activa desde un animationEvent
    public void mover()
    {
        transform.parent.GetComponent<Seta>().moverse = true;
    }
}
