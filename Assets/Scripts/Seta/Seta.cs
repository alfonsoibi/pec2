﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seta : MonoBehaviour
{
    //Componentes
    private Rigidbody2D rb;

    //Control
    public float velocidad;
    public bool moverse;

    //Dirección
    bool setaDerecha =  false;
    bool setaIzquierda = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        moverse = false;
        velocidad = 3;
    }

    void Update()
    {
        //La seta empezará a moverse cuando se ejecute la función del animationEvent
        if (moverse == true){
            rb.velocity = new Vector2(velocidad, rb.velocity.y);  
        }
    }

    private void FixedUpdate()
    {
        //Creamos un rayo a cada lado de la seta para detectar la colision con las plataformas
        RaycastHit2D hitDerecha = Physics2D.Raycast(new Vector2(transform.position.x + (transform.localScale.x / 2 + 0.01f), transform.position.y), Vector2.right, 0.1f);
        RaycastHit2D hitIzquierda = Physics2D.Raycast(new Vector2(transform.position.x - (transform.localScale.x / 2 + 0.01f), transform.position.y), Vector2.left, 0.1f);
        //Dibujamos los rayos en el editor para ver que están situados correctamente
        Debug.DrawRay(new Vector2(transform.position.x + (transform.localScale.x / 2 + 0.01f), transform.position.y), Vector2.right, Color.red, 0.1f);
        Debug.DrawRay(new Vector2(transform.position.x - (transform.localScale.x / 2 + 0.01f), transform.position.y), Vector2.left, Color.green, 0.1f);
        
        //Si la seta choca con una plataforma cambiamos la dirección modificando la velocidad
        if (hitIzquierda.collider != null && !setaIzquierda && hitIzquierda.collider.gameObject.layer == 8){
            velocidad *= -1;
            setaIzquierda = true;
            setaDerecha = false;
        }
        if (hitDerecha.collider != null && !setaDerecha && hitDerecha.collider.gameObject.layer == 8)
        {
            velocidad *= -1;
            setaIzquierda = false;
            setaDerecha = true;
        }    
    }
}
