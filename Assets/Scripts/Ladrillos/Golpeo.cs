﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Golpeo : MonoBehaviour
{

    //Componentes
    Animator anim;

    //Audio
    AudioSource audio;

    //Scripts
    MovimientoMario player;
    ControlNivel control;

    //Control
    public bool destruible;
    public bool tocado;
    
    void Start()
    {
        audio = GameObject.Find("MARIO").GetComponent<AudioSource>();
        
        control = GameObject.Find("Controlador").GetComponent<ControlNivel>();
        player = GameObject.Find("MARIO").GetComponent<MovimientoMario>();
        anim = GetComponentInParent<Animator>();
    }

    void Update()
    {
        //Comprobamos el estado de Mario para sabe si romperá el bloque o no
        if (player.estado > 0){
            destruible = true;
        }
        else{
            destruible = false;
        }
        if (tocado) {

            tocado = false;
            if (destruible)
            {
                audio.PlayOneShot(player.golpeo);
                //Destruimos el bloque
                anim.SetBool("Destruido", true);
                //Añadimos los puntos al controlador general
                control.puntos = control.puntos + control.puntosBloque;
            }
            else {
                audio.PlayOneShot(player.golpeo);
                //Sólo se golpea el bloque
                anim.SetTrigger("Golpeado");
            }
        }
    }
}
