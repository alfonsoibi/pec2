﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activador : MonoBehaviour
{
    //Detectams cuando Mario rebasa el activador
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.name == "MARIO")
        {
            //Hacemos que el Goomba empiece a moverse
            transform.parent.GetComponent<goomba>().moverse = true;
            //Eliminamos el activador
            Destroy(this.gameObject);
        }
    } 
}
