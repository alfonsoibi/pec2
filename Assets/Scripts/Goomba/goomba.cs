﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goomba : MonoBehaviour
{
    //Componentes
    private Animator anim;
    private Rigidbody2D rb;
    public Transform spriteGoomba;

    //Control
    public float velocidad;
    public bool moverse = false;
    bool goombaDerecha = false;
    bool goombaIzquierda = false;
    public bool eliminado = false;

    void Start()
    {
        anim = spriteGoomba.GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        moverse = false;
        velocidad = -2;
    }

    void Update()
    {
        //Si Mario ha saltado sobre la seta, lanzamos la animacion de eliminar
        if (eliminado) {
            anim.SetBool("Eliminado", true);
        //Si Mario ha chocado con el activador hacemos que el Goomba empiece a moverse
        }else if (moverse == true){
            rb.velocity = new Vector2(velocidad, rb.velocity.y);
        }
    }

    private void FixedUpdate()
    {
        //Creamos un rayo a cada lado del Goomba para detectar las colisiones laterales
        RaycastHit2D hitDerecha = Physics2D.Raycast(new Vector2(transform.position.x + (transform.localScale.x / 2 + 0.1f), transform.position.y+0.5f), Vector2.right, 0.1f);
        RaycastHit2D hitIzquierda = Physics2D.Raycast(new Vector2(transform.position.x - (transform.localScale.x / 2 + 0.1f), transform.position.y + 0.5f), Vector2.left, 0.1f);
        //Dibujamos los rayos en el editor para comprobar que estén bien posicionados
        Debug.DrawRay(new Vector2(transform.position.x + (transform.localScale.x / 2 + 0.1f), transform.position.y + 0.5f), Vector2.right, Color.red, 0.1f);
        Debug.DrawRay(new Vector2(transform.position.x - (transform.localScale.x / 2 + 0.1f), transform.position.y + 0.5f), Vector2.left, Color.green, 0.1f);
        //cuando el Goomba choca con otro Goomba o con una plataforma, cambia de dirección
        if (hitIzquierda.collider != null && !goombaIzquierda && (hitIzquierda.collider.gameObject.layer == 8 || hitIzquierda.collider.gameObject.layer == 12)){
            velocidad *= -1;
            goombaIzquierda = true;
            goombaDerecha = false;
        }
        if (hitDerecha.collider != null && !goombaDerecha && (hitDerecha.collider.gameObject.layer == 8 || hitDerecha.collider.gameObject.layer == 12)){
            velocidad *= -1;
            goombaIzquierda = false;
            goombaDerecha = true;
        }
    }
}
