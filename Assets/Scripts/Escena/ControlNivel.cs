﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlNivel : MonoBehaviour
{

    GameObject Datos;

    //audio
    public AudioSource audioNivel;
    public AudioClip fuegosArtificiales;
    public AudioClip sonidoEliminado;

    //Datos
    public Text tMonedas;
    public int monedas;
    public Text tPuntos;
    public int puntos;
    public int puntosMoneda;
    public int puntosGoomba;
    public int puntosPowerUp;
    public int puntosBloque;

    //Tiempo
    public Text cronometro;
    public float tiempo = 400;

    //Objetos
    public GameObject fuego1;
    public GameObject fuego2;
    public GameObject fuego3;

    private void Start()
    {
        audioNivel = GetComponent<AudioSource>();
        Datos = GameObject.Find("Datos");
    }

    void Update()
    {
        //Mostramos los valores en el UI
        tMonedas.text = "x" + monedas.ToString("00");
        tPuntos.text = puntos.ToString("000000");
        tiempo -= Time.deltaTime;
        cronometro.text = tiempo.ToString("f000");
        if (Datos.GetComponent<DatosEntreEscenas>().meta) {
            Datos.GetComponent<DatosEntreEscenas>().meta = false;
            audioNivel.clip = sonidoEliminado;
            Datos.GetComponent<DatosEntreEscenas>().puntosTotales = Datos.GetComponent<DatosEntreEscenas>().puntosTotales + puntos;
            StartCoroutine(Fin());

        }
        
    }
    private IEnumerator Fin()
    {
        fuego1.SetActive(true);
        audioNivel.PlayOneShot(fuegosArtificiales);
        yield return new WaitForSeconds(1);
        fuego2.SetActive(true);
        audioNivel.PlayOneShot(fuegosArtificiales);
        yield return new WaitForSeconds(1);
        fuego3.SetActive(true);
        audioNivel.PlayOneShot(fuegosArtificiales);
        yield return new WaitForSeconds(3);
        Destroy(Datos);
        SceneManager.LoadScene("Inicio");
    }
}