﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlTransicion : MonoBehaviour
{

    public Text tVidas;
    DatosEntreEscenas Datos;

    void Start()
    {
        //Obtenemos el número de vidas del jugador
        Datos = GameObject.Find("Datos").GetComponent<DatosEntreEscenas>();
        tVidas.text = "x " + Datos.vidas.ToString();
        StartCoroutine(CargarNivel());
    }

    private IEnumerator CargarNivel()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("Nivel");

    }
}
