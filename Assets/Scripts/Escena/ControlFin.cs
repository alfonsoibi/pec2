﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlFin : MonoBehaviour
{
    GameObject Datos;

    private void Start()
    {
        StartCoroutine(Fin());
        GameObject.Find("Datos");
    }

    private IEnumerator Fin()
    {
        yield return new WaitForSeconds(3);
        Destroy(Datos);
        SceneManager.LoadScene("Inicio");

    }
}
