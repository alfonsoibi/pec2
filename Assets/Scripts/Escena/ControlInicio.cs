﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlInicio : MonoBehaviour
{
    //Audio
    AudioSource audio;
    public AudioClip clic;
    public AudioClip ok;

    DatosEntreEscenas Datos;

    //Interfaz
    public GameObject imgSeleccion;
    public int seleccion;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        Datos = GameObject.Find("Datos").GetComponent<DatosEntreEscenas>();
        seleccion = 0;
        Datos.vidas = 3;
    }

    void Update()
    {
        //Controlamos el menú con las teclas de dirección.
        if (Input.GetKeyDown(KeyCode.DownArrow) && seleccion == 0){
            audio.PlayOneShot(clic);
            seleccion++;
            imgSeleccion.transform.position = new Vector2(imgSeleccion.transform.position.x, imgSeleccion.transform.position.y - 44);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && seleccion == 1){
            audio.PlayOneShot(clic);
            seleccion--;
            imgSeleccion.transform.position = new Vector2(imgSeleccion.transform.position.x, imgSeleccion.transform.position.y + 44);
        }
        //Pulamos "espacio" para seleccionar la opcion
        if (Input.GetKeyDown(KeyCode.Space)){
            if (seleccion == 0){
                iniciar();
            }
            else{
                salir();
            }
        }
    }
    //Pasamos a la escena de transicion
    public void iniciar()
    {
        StartCoroutine(Empezar());
        
    }
    //Salimos de la aplicación
    public void salir()
    {
        Application.Quit();
    }
    private IEnumerator Empezar()
    {

        audio.PlayOneShot(ok);
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("Transicion");
    }
}
