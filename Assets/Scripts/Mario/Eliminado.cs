﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Eliminado : MonoBehaviour
{
    DatosEntreEscenas Datos;

    // Start is called before the first frame update
    void Start()
    {
        Datos = GameObject.Find("Datos").GetComponent<DatosEntreEscenas>();
        Datos.vidas--;
        
    }

    private void Update()
    {
        if (Datos.vidas > 0){
            StartCoroutine(CargarTransicion());
        }
        else {

            StartCoroutine(CargarMenu());
        }
    }

    private IEnumerator CargarTransicion()
    {
      
    yield return new WaitForSeconds(3);
    SceneManager.LoadScene("Nivel");
    }
    private IEnumerator CargarMenu()
    {

        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("Inicio");
    }
}
