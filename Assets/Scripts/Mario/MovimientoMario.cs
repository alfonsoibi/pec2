﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class MovimientoMario : MonoBehaviour
{
    //Controlador del juego
    ControlNivel controlador;
    DatosEntreEscenas Datos;

    //Audio
    public AudioClip salto;
    public AudioClip bloqueado;
    public AudioClip golpeo;
    public AudioClip moneda;
    public AudioClip transformacion;
    public AudioSource audio;

    //Componentes
    private Animator anim;
    private Rigidbody2D rb;

    //Movimiento
    public float velocidad;
    public float velocidadInicial;
    public float velocidadCorrer;
    public float fuerzaSalto;
    float movimiento;
    bool agachado;

    //Estado
    public int estado; //0-Mario pequeño 1-Mario Grande 2-Mario Disparo

    //En el suelo
    bool enElSuelo = false;

    //Mejora salto
    public float multiplicadorCaida = 2.5f;
    public float multiplcadorSaltoPequeño = 2f;

    //Caida
    float ultimaVezEnSuelo;

    //Dirección
    private bool mirarDerecha;

    //otros
    public int puntosBandera;
    public bool final;
    public bool avanzar;
    public GameObject objetoBandera;
    
    //Primero, obtenemos en controlador de la escena.
    private void Awake()
    {
        controlador = GameObject.Find("Controlador").GetComponent<ControlNivel>();
        Datos = GameObject.Find("Datos").GetComponent<DatosEntreEscenas>();
    }
    void Start()
    {
        audio = GetComponent<AudioSource>();
        //cargamos el rigidbody de Mario
        rb = GetComponent<Rigidbody2D>();
        //cargamos el animator de Mario
        anim = GetComponent<Animator>();
        //Establecemos la velocidad
        velocidad = velocidadInicial;
        //Establecemos la dirección hacia la que mira Mario.
        mirarDerecha = true;
    }

    void Update()
    {
        //Dependiendo del estado de mario, modificamos las animaciones para que aparezca Mario pequeño, Mario grande o Mario con Disparo.
        switch (estado)
            {
                case 0:
                    anim.SetBool("Grande", false);
                    anim.SetBool("Disparo", false);
                    break;
                case 1:
                    anim.SetBool("Grande", true);
                    anim.SetBool("Disparo", false);
                    break;
                case 2:
                    anim.SetBool("Grande", true);
                    anim.SetBool("Disparo", true);
                    break;
                default:
                    Debug.Log("Estado fuera de rango");
                    break;
            }
            MovimientoHorizontal();
            Salto();
            correr();
            agacharse();
            MejorarSalto();
            Direccion(movimiento);    
    }

    //El FixedUpdate, se ejecutará en sincronización con el motor de físicas, independientemente de la carga del juego.
    private void FixedUpdate()
    {
        comprobarSuelo();     
    }

    //Hacemos que Mario se mueva horizontalmente con las flechas, excepto si está agachado.
    void MovimientoHorizontal()
    {
        if (!final)
        {
            if (!agachado)
            {
                float x = Input.GetAxisRaw("Horizontal");
                movimiento = x * velocidad;
                rb.velocity = new Vector2(movimiento, rb.velocity.y);
                anim.SetFloat("velocidad", Mathf.Abs(movimiento));
            }
        }
        else {

            if (avanzar) {
                rb.velocity = new Vector2(velocidad, rb.velocity.y);
                   
            }
        
        }
    }

    //Hacemos que el jugador corra al mantener la tecla "Control" pulsada.
    void correr() {
        if (Input.GetKey(KeyCode.LeftControl) && movimiento!=0 && enElSuelo){        
            velocidad = velocidadCorrer;    
        }
        if (Input.GetKeyUp(KeyCode.LeftControl)) {   
            velocidad = velocidadInicial;
        }
    }

    //Hacemos que Mario pueda agacharse
    void agacharse() {
        if (Input.GetKey(KeyCode.DownArrow) && enElSuelo) {
            anim.SetBool("Agachado", true);
            agachado = true;      
            //En el caso de que estemos moviéndonos, haremos que al agacharse la velocidad desminuya lentamente. Así, Mario deslizará un breve periodo de tiempo.
            rb.velocity = new Vector2(rb.velocity.x * 0.95f, rb.velocity.y);
        }
        if (Input.GetKeyUp(KeyCode.DownArrow)){
            agachado = false;
            anim.SetBool("Agachado", false);
        }
    }

    //Hacemos que Mario salte, siemrpe y cuando esté tocando el suelo y no esté eliminado
    void Salto(){     
            if (Input.GetKeyDown(KeyCode.Space) && (enElSuelo) && (estado >= 0)){

            audio.PlayOneShot(salto);
            if (Input.GetKey(KeyCode.LeftControl)){
                rb.velocity = new Vector2(rb.velocity.x, fuerzaSalto+1);
            } else {
                rb.velocity = new Vector2(rb.velocity.x, fuerzaSalto);
            }
            
        }
    }

    //Evitamos la sensacion de flotación en el salto, mediante esta función, hacemos que mario caiga más rapido de lo que tarda en subir
    void MejorarSalto(){
        if (rb.velocity.y < 0){
            rb.velocity += Vector2.up * Physics2D.gravity * (multiplicadorCaida - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !Input.GetKey(KeyCode.Space)){
            rb.velocity += Vector2.up * Physics2D.gravity * (multiplcadorSaltoPequeño - 1) * Time.deltaTime;
        }
    }

    //Comprobamos si Mario está tocando el suelo
    void comprobarSuelo()
    {
        //Creamos dos rayos bajo Mario para detectar el suelo.
        RaycastHit2D AbajoHit1 = Physics2D.Raycast(new Vector2(transform.position.x - transform.localScale.x / 4, transform.position.y), Vector2.down, 0.1f);
        RaycastHit2D AbajoHit2 = Physics2D.Raycast(new Vector2(transform.position.x + transform.localScale.x / 4, transform.position.y), Vector2.down, 0.1f);
        //Dibujamos los rayos en el editor para comprobar su posición
        Debug.DrawRay(new Vector2(transform.position.x - transform.localScale.x / 4, transform.position.y), Vector2.down, Color.red, 0.01f);
        Debug.DrawRay(new Vector2(transform.position.x + transform.localScale.x / 4, transform.position.y), Vector2.down, Color.red, 0.01f);
        //Comprobamos si los rayos están chocando contra la capa "Plataforma"
        if ((AbajoHit1.collider != null && AbajoHit1.collider.gameObject.layer == 8) || (AbajoHit2.collider != null && AbajoHit2.collider.gameObject.layer == 8)){
            //Toca el suelo
            enElSuelo = true;
            anim.SetBool("Salto", false);
        }
        else {
            if (enElSuelo){
                ultimaVezEnSuelo = Time.time;
            }
            enElSuelo = false;
            anim.SetBool("Salto", true);
        }   
    }

    //Mediante esta funcion eliminamos a los enemigos
    private void EliminarEnemigo(Transform enemigo) { 
        //Hacemos que deje de moverse
        enemigo.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        //Deshabilitamos el collider para evitar que Mario siga chocando
        enemigo.GetComponent<Collider2D>().enabled = false;
        //Hacemos que Mario de un pequeño salto hacia arriba
        if (rb.velocity.y <= 0) {
            
            rb.AddForce(transform.up * 450);
        }
        //Lanzamos la animación 
        enemigo.GetComponentInChildren<Animator>().SetBool("Eliminado", true);
        controlador.puntos = controlador.puntos + controlador.puntosGoomba;
    }

    //Mediente esta función, cambiamos la dirección en la que mira el Sprite de Mario.
    private void Direccion(float movimiento) { 
        if(movimiento > 0 && !mirarDerecha || movimiento < 0 && mirarDerecha){
            mirarDerecha = !mirarDerecha;
            transform.rotation = Quaternion.Euler(new Vector2(transform.eulerAngles.x, transform.eulerAngles.y + 180f));
            }
    }
    //Al caer por un hueco o chocar con un enemigo siendo pequeño ejecutamos esta funcion
    private void Eliminado()
    {
        //Congelamos el movimiento en x
        rb.constraints = RigidbodyConstraints2D.FreezePositionX; 
        rb.AddForce(transform.up * 500);
        GetComponent<Collider2D>().enabled = false;
        //Lanzamos la animacion de eliminar a Mario
        anim.SetBool("Eliminado", true);
        if (Datos.vidas > 1){
            StartCoroutine(CargarTransicion());
        }
        else{
            StartCoroutine(CargarFinal());
        }
    }
    //Cargamos la siguiente escena cuando eliminan a Mario
    private IEnumerator CargarTransicion(){
        yield return new WaitForSeconds(3.5f);
        Datos.vidas--;
        SceneManager.LoadScene("Transicion");
    }
    private IEnumerator CargarFinal(){

        yield return new WaitForSeconds(3.5f);
        SceneManager.LoadScene("Final");
    }
    //Cuendo colisionamos con un enemigo, ignaramos la colision durante unos segundos para evitar más colisiones.
    private IEnumerator tocado() {
        Physics.IgnoreLayerCollision(9,12, true);
        yield return new WaitForSeconds(2);
        Physics.IgnoreLayerCollision(9, 12, false);
    }

    private IEnumerator bandera(GameObject bandera) {

        rb.constraints = RigidbodyConstraints2D.FreezePosition;
        bandera.GetComponent<Collider2D>().isTrigger = true;
        final = true;
        puntosBandera = (int)(transform.position.y * 10000) / 2;
        yield return new WaitForSeconds(0.5f);
        rb.constraints = RigidbodyConstraints2D.None;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        rb.AddForce(transform.up);
        bandera.GetComponent<Animator>().SetBool("Bajar", true);
        yield return new WaitForSeconds(1f);
        avanzar = true;
        Datos.puntosTotales = Datos.puntosTotales + puntosBandera;
    }

    //COLISIONES DEL PERSONAJE
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Detectamos la colisión con un bloque de ?
        if (collision.gameObject.layer == 10) {
            collision.gameObject.GetComponent<CantidadMonedas>().tocado = true;
        }
        //Detectamos la colisión con un bloque de ladrillo
        if (collision.gameObject.layer == 11) {
            collision.gameObject.GetComponent<Golpeo>().tocado = true;
        }
        //Detectamos la colisión con un enemigo
        if (collision.gameObject.layer == 12) {  
                //Cambiamos el estado de Mario
                if (estado > 0){
                    estado--;
                audio.PlayOneShot(transformacion);
                    StartCoroutine(tocado());    
                      
                }
                else{
                controlador.audioNivel.Stop();
                controlador.audioNivel.clip = controlador.sonidoEliminado;
                controlador.audioNivel.Play();
                Eliminado();
                }
        }
        //Detectamos la colisión con la parte superior del enemigo.
        if (collision.gameObject.name == "GoombaSprite"){
            EliminarEnemigo(collision.transform);
        }       
        //Detectamos la colision con la seta
        if (collision.gameObject.name == "Seta") {
            Destroy(collision.gameObject);
            audio.PlayOneShot(transformacion);
            //Cambiamos el estado de Mario
            if (estado < 3) {               
                estado++;   
            }
            controlador.puntos = controlador.puntos + controlador.puntosPowerUp;
        }
        //Detectamos la colision con la Flor
        if (collision.gameObject.name == "Flor"){
            Destroy(collision.gameObject);
            audio.PlayOneShot(transformacion);
            //Cambiamos el estado de Mario
            if (estado < 3){
                estado++;
            }
            controlador.puntos = controlador.puntos + controlador.puntosPowerUp;
        }
        //Detectamos la colision con la bandera
        if (collision.gameObject.name == "Bandera")
        {
            StartCoroutine(bandera(collision.gameObject));
        }
        if (collision.gameObject.name == "Fin")
        {
            Datos.meta = true;
            rb.constraints = RigidbodyConstraints2D.FreezePosition;

        }

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 14) {
            Eliminado();
        }
    }
}
