﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CantidadMonedas : MonoBehaviour
{
    //Componentes
    Animator anim;

    //Audio
    AudioSource audio;

    //Scripts
    MovimientoMario player;

    //Objetos
    ControlNivel controlador;
    public GameObject moneda;
    public GameObject PowerSeta;
    public GameObject PowerFlor;

    //Numero de veces que Mari opuede golpear la casilla
    public int cantidadGolpes;

    //Control
    public bool tocado;
    public bool casillaPowerUp;
    int estadoJugador;

    void Start()
    {
        audio = GameObject.Find("MARIO").GetComponent<AudioSource>();
        player = GameObject.Find("MARIO").GetComponent<MovimientoMario>();
        //Accedemos al controlador de la escena
        controlador = GameObject.Find("Controlador").GetComponent<ControlNivel>();
        anim = GetComponentInParent<Animator>();
    }

    void Update()
    {
        //Comprobamos el estado de Mario, si es grande, pequeño o tiene el disparo
        estadoJugador = GameObject.Find("MARIO").GetComponent<MovimientoMario>().estado;
        //Comprobamos si se ha alcanzado el numero de golpes
        if (cantidadGolpes > 0) {
            if (tocado) {

                tocado = false;
                //Si es una casilla que esconde una Seta o una Flor
                if (casillaPowerUp){
                    audio.PlayOneShot(player.golpeo);
                    if (estadoJugador == 0){
                        //Si Mario aun es pequeño instanciamos una seta
                        GameObject seta = Instantiate(PowerSeta, new Vector2(transform.position.x, transform.position.y + 1), transform.rotation);
                        seta.name = "Seta";
                    }else {
                        //Si Mario es grande instanciamos la flor
                        GameObject flor = Instantiate(PowerFlor, new Vector2(transform.position.x, transform.position.y + 1), transform.rotation);
                        flor.name = "Flor";
                    }
                }
                else{
                    audio.PlayOneShot(player.moneda);
                    //Si la casilla no esconde ni la seta ni la flor, instanciamos una moneda
                    Instantiate(moneda, transform.position, transform.rotation);
                    //Sumamos puntos y la moneda al contador general
                    controlador.monedas++;
                    controlador.puntos = controlador.puntos + controlador.puntosMoneda;
                }
                anim.SetTrigger("Golpeada");
                anim.SetBool("Bloqueada", false);
                cantidadGolpes--;
            }
        }
        else{
            
            anim.SetBool("Bloqueada", true);
        }
    }
}
