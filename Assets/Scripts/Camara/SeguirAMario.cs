﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

public class SeguirAMario : MonoBehaviour
{
    public Transform Player;

    private Vector3 playerPos;
    private Vector3 cameraPos;

    public float velocidad;

    float inicio;

    private void Start()
    {
       
        inicio = transform.position.x;
    }

    private void Update()
    {
        
        if (Player.position.x > inicio) {

            playerPos = new Vector3(Player.transform.position.x, transform.position.y, transform.position.z);
            

            transform.position = Vector3.Lerp(transform.position, playerPos, velocidad * Time.deltaTime);


            
        }
        

    }
}
