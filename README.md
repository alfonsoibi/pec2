**PEC 2 - Un juego de plataformas**

Configuración de teclas:

- Selección Menu -> Flechas arriba/abajo
- Mover a la derecha -> Flecha derecha
- Mover a la izquierda -> Flecha izquierda
- Agacharse -> Flecha abajo
- Saltar/aceptar -> Barra espaciadora
- correr -> Ctrl Izquierda

Las reglas del juego son exactamente las mismas que el juego de Super Mario Bros. 
Una vez empieza el nivel, el jugador puede mover a mario con las flechas de dirección. Tendrá que avanzar hacia la derecha y llegar hasta el castillo final. Podrá saltar, correr, avanzar, retroceder, agacharse y golpear las plataformas. Si choca con cualquier enemigo y mario es aun pequeño o cae por un hueco del escenario, perderá una vida. Si pierde tres vidas termina la partida.
El jugador podrá eliminar a los enemigos saltando sobre ellos. Si golpea las casillas marcadas cxon un interrogante, logrará monedas o potenciadores. Hay dos tipos de potenciadores:
- La seta: Mario crece y puede ser golpeado por un enemigo una vez más.
- La flor: Mario adquiere el disparo (No implementado)y podrá ser golpeado otra vez más por los enemigos.
Cuando el jugador llega a la bandera final, cuanto ma´s alto choque contra ella, mayor será la puntuación que reciba.
Se obtienen puntos por eliminar enemigos, recoger monedas, recoger potenciadores o destruir bloques del escenario.

Aspectos generales:

Estructura:

    Inicio - Menu inicial (Creamos el objeto Datos, que se mantendrá entre el resto de escenas)
    Transición - Se muestran las vidas que tiene el jugador (Se consulta el objeto Datos)
    Nivel - Es la escena dónde se desarrolla el juego
    Final - Es la escena que se carga cuando se pierden todas las vidas.
    
    Controladores: En cada escena, se ha creado un objeto vacío con un script que controla los aspectos generales de la escena.
Escenario:

    El escenario se ha creado utilizando un Tilemap. Se ha dibujado el suelo y el decorado mediante el TilePalette.
    Dentro del objeto Grid, se ha creado el decorado que queda detrás, y las plataformas sobre las que se mueve el personaje.

    Las plataformas destruibles y las casillas con el interrogante se han creado como sprites independientes.

Personaje:

    Se ha añadido un rigidbody2D, un collider2D, un Animator Controller y un script para al personaje.

    En primer lugar, se han definido tres estados para el personaje(Pequeño, Grande, Disparo). Después se han definido las animaciones del personaje en cada uno de estos estados. Mediante el Animator Controller, se van lanzando las diferentes animaciones dependiendo del estado del personaje.

    Para correr, simplemente se controla cuando el jugador ha pulsado la tecla correspondiente.

    Al agacharse el jugador, se reduce paulativamente la velocidad del mismo para que se deslice ligeramente si estaba en movimiento.

    Para el salto, se comprueba primero que el jugador esté tocando el suelo. Además, se ha modificado la física para que caiga más rápido de lo que sube, de este modo se consigue un salto más similar al del juego original.

    Para comprobar que Mario está en el suelo, se han creado dos rayos hacia abajo partiendo de los pies del personaje. De este modo, se comprueba si está en contacto con la plataforma.

    Para el resto de colisiones, se utiliza el collider2D.

    Cuando Mario choca contra un enemigo, se cambia el estado o se elimina a Mario si es pequeño aun. Cuando choca contra un potenciador, tambien se modifica su estado.

Enmigos:

    Los enemigos se han colocado por el escenario y cada uno tiene un activador. Cuado Mario choca contra un activador, el enemigo comienza a moverse.
    
    Se ha creado un rayo a cada lado de los enemigos para detectar las colisiones laterales con el escenario, así, cambian de dirección al chocar contra una pared.

    Los enemigos tienen dos colliders, apra detectar si Mario choca con ellos por arriba o por cualquier otro lado.

    Si Mario salta sobre ellos, los elimina. Si Mario choca por cualquier otra parte, cambiará de estado o quedará eliminado.
    
Audio:

    Se han añadido efectos de sonido y la música de fondo del juego original de Super Mario Bros.

ID del repositorio Gitlab: https://gitlab.com/alfonsoibi/pec2
Enlace al vídeo explicativo: https://youtu.be/92vwrR6Z7As 
